# Configuration for ticket types

This allows to configure the ticket types which can be used in GoTi.

Example:

```yaml
ticket_type:
  - key: SD
    display: ticket_type.sd.service_desks
    states: ticket_state.default
    process: ticket_process.default
    priorities:
      - Critical
      - High
      - Medium
      - Low
    fields:
      - key: time_recording
        display: ticket_type.sd.time_recording
        type: number
        default: 1
        required: true
      - key: classification
        display: ticket_type.sd.classification
        type: select
        values:
          - ticket_type.sd.classification.knowledge_issue
          - ticket_type.sd.classification.documentation_issue
```

The configuration item 'ticket_type' contains a list of all available types in the system.
This example creates a type for the Service Desk.

## Description of the configuration options

### Ticket type

A ticket type has the following configuration options:


| Option     | Mandatory | Description |
|------------|-----------|-------------|
| key        | yes       | Defines the unique key of this ticket type. Minimum of 2, maximum of 5 characters (Alpha only) |
| display    | yes       | Defines the display name of the ticket type. It has to be a translation key |
| states     | yes       | Key of the state definition used for this ticket type |
| process    | yes       | Key of the process definition used for this ticket type |
| priorities | yes       | Defines the list of available priorities. The list has to be ordered from highest to lowest priority |
| fields     | no        | A list of custom fields for this ticket type. By default a ticket always has a summary and a description. Additional fields kan be added here |

### Fields

The field configuratino allows to add your own custom fields to your ticket type.
A field has the following configuration options:

| Option     | Mandatory | Description |
|------------|-----------|-------------|
| key        | yes       | The unique identifier for this field |
| display    | yes       | Defines the display name of the field. It has to be a translation key |
| type       | yes       | The type of the field. |
| default    | no        | The default value of the field |
| required   | no        | Defines if the field is mandatory, default is false |
| values     | yes, for type 'select' | Defines the values for the select list. Is a list of translation keys which will represent the values |

#### Field types

Field types can be one of the following:

* Boolean
  * true or false
* Date
  * Allows to set a date using a date picker
* Datetime
  * Allows to set a date and tme using a date time picker
* Number
  * Allows to set a number
* Text
  * Allows to set an aribtrary text
* User
  * Allows to set a user with a user picker
* Group
  * Allows to set a user group with a group picker
* Select
  * Allows to create a select box with predefined values