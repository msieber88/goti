# Configuration for ticket states

This allows to configure the ticket states for the ticket types.

Example:

```yaml
ticket_state:
  - key: ticket_state.default
    states:
      - key: open
        display: ticket_state.default.open
      - key: inprogress
        display: ticket_state.default.inprogress
      - key: closed
        display: ticket_state.default.closed
```

The configuration item 'ticket_state' contains a list of all available states for the ticket types in the system.
This example creates the state for the Service Desk ticket type.

## Description of the configuration options

### Ticket state

A ticket type has the following configuration options:


| Option     | Mandatory | Description |
|------------|-----------|-------------|
| key        | yes       | Defines the unique key of this ticket state |
| states     | yes       | A list of states for this ticket type |

### States

The state configuratino allows to add your own states to your ticket type.
A state has the following configuration options:

| Option     | Mandatory | Description |
|------------|-----------|-------------|
| key        | yes       | The unique identifier for this state |
| display    | yes       | Defines the display name of the state. It has to be a translation key |