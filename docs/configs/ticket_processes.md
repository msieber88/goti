# Configuration for ticket processes

This allows to configure the ticket processes for the ticket types.

Example:

```yaml
ticket_process:
  - key: ticket_process.default
    display: ticket_process.default.name
    transitions:
      - key: from_open_to_inprogress
        display: ticket_process.default.from_open_to_inprogress
        from_state: open
        to_state: inprogress
      - key: from_inprogress_to_close
        display: ticket_process.default.from_inprogress_to_close
        from_state: inprogress
        to_state: closed
        condition: ??time_recording
```

The configuration item 'ticket_process' contains a list of all available processes for the ticket types in the system.
This example creates the process for the Service Desk ticket type.

## Description of the configuration options

### Ticket process

A ticket type has the following configuration options:


| Option      | Mandatory | Description |
|-------------|-----------|-------------|
| key         | yes       | Defines the unique key of this ticket process |
| display     | yes       | Defines the display name of the ticket process. It has to be a translation key |
| transitions | yes       | A list of transitions for this ticket type |

### Transitions

The transition configuratino allows to add your own transitions to your ticket process.
A transition has the following configuration options:

| Option     | Mandatory | Description |
|------------|-----------|-------------|
| key        | yes       | The unique identifier for this transition |
| display    | yes       | Defines the display name of the transition. It has to be a translation key |
| from_state | yes       | Defines from which state this transition can be triggered. This has to match the key of the transition of this ticket type |
| to_state   | yes       | Defines to which state this transition will lead (the target state after the transition). This has to match the key of the transition of this ticket type |
| condition  | no        | Defines the condition, when this transition can be triggered |

### Conditions

A condition determines when a given transition can be performed.
The evaluation of the condition is done by the following library: https://github.com/PaesslerAG/gval

Conditions can contain the following elements:

* Modifiers: + - / * & | ^ ** % >> <<
* Comparators: > >= < <= == != =~ !~
* Logical ops: || &&
* Numeric constants, as 64-bit floating point (12345.678)
* String constants (double quotes: "foobar")
* Date function 'Date(x)', using any permutation of RFC3339, ISO8601, ruby date, or unix date
* Boolean constants: true false
* Parentheses to control order of evaluation ( )
* Json Arrays : [1, 2, "foo"]
* Json Objects : {"a":1, "b":2, "c":"foo"}
* Prefixes: ! - ~
* Ternary conditional: ? :
* Null coalescence: ??

The following variables can be accessed by a condition:

* field values of the ticket by using the name of the field:
  * summary
  * description
  * created
  * updated
  * assignee
  * activity
* custom field values of the ticket by using the given key as the variable name
* current user object with groups