# Configuration for ticket states

This allows to configure basic settings.

Example:

```yaml
common:
  server:
    host: localhost
    port: 8080

  database:
    host: localhost
    port: 5432
    database: goti
    user: user
    password: password

  mail:
    sender: goti@localhost.local
    smtp:
      user: goti
      password: password
      host: smtp.goti.com
      port: 587

  authentication:
    type: 
      - default
    default:
      allowRegistration: true
```

## Description of the configuration options

### Server

The 'server' has the following configuration options:


| Option     | Mandatory | Default   | Description |
|------------|-----------|-----------|-------------|
| host       | no        | localhost | Defines the host on which the server is listening |
| port       | no       | 8080      | Defines the port on which the server is listening |

### Database

The 'database' has the following configuration options:


| Option     | Mandatory | Default   | Description |
|------------|-----------|-----------|-------------|
| host       | no        | localhost | The host on which the postgres database is running |
| port       | no        | 5432      | The port on which the postgres database is listening |
| database   | no        | goti      | The name of the database |
| user       | no        | goti      | The user to connect to the postgres database |
| password   | no        |           | The password of the connecting user |

### Mail

The 'mail' has the following configuration options:


| Option     | Mandatory | Default   | Description |
|------------|-----------|-----------|-------------|
| sender     | yes       |           | The mail address which is used as the sender for all notifications |
| smtp       | yes       |           | Configuration of the SMTP server |

#### SMTP

The 'smtp' has the following configuration options:


| Option     | Mandatory | Default   | Description |
|------------|-----------|-----------|-------------|
| user       | no        |           | The user for authentication with the SMTP server |
| password   | no        |           | The password for authentication with the SMTP server |
| host       | no        | localhost | The host on which the SMTP server is running |
| port       | no        | 587       | The SMTP server port |

### Authentication

The 'authentication' has the following configuration options:


| Option     | Mandatory | Default   | Description |
|------------|-----------|-----------|-------------|
| secret     | yes       |           | The secret used to sign the JWT authentication token. Requires a minimum length of 20 characters |
| type       | yes       |           | List of used authentication methods (currently only 'default') |
| default    | no        |           | Configuration of the default authentication system. Must be set if 'type' contains 'default' as authentication method |

#### Default Authentication

The 'default' has the following configuration options:


| Option     | Mandatory | Default   | Description |
|------------|-----------|-----------|-------------|
| allowRegistration | no       | true          | Defines if user self registration is allowed |