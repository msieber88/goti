package ticket

import (
	"gitlab.com/msieber88/goti/auth"
	"gorm.io/gorm"
)

// Ticket defines the database model for ticket data
type Ticket struct {
	gorm.Model
	Key         string `gorm:"primaryKey"`
	Summary     string
	Description string
	Creator     auth.User
	State       string
	Activities  []Activity
}

// Activity holds all logged activities and comments of a ticket
type Activity struct {
	gorm.Model
	Message string
	Creator auth.User
}
