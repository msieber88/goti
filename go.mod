module gitlab.com/msieber88/goti

go 1.15

require (
	github.com/creasty/defaults v1.5.1
	github.com/fsnotify/fsnotify v1.4.9
	github.com/go-chi/chi v1.5.1
	github.com/go-chi/jwtauth v1.1.1
	github.com/go-playground/validator/v10 v10.4.1
	github.com/jackc/pgproto3/v2 v2.0.7 // indirect
	github.com/jessevdk/go-flags v1.4.0
	github.com/sirupsen/logrus v1.7.0
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.6.1 // indirect
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad // indirect
	golang.org/x/sys v0.0.0-20210113181707-4bcb84eeeb78 // indirect
	golang.org/x/text v0.3.5 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
	gorm.io/driver/postgres v1.0.6
	gorm.io/gorm v1.20.11
)
