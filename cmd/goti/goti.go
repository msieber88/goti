package main

import (
	"os"
	"path"
	"path/filepath"

	"github.com/jessevdk/go-flags"
	log "github.com/sirupsen/logrus"
	"gitlab.com/msieber88/goti/configs"
	"gitlab.com/msieber88/goti/database"
	"gitlab.com/msieber88/goti/server"
)

func main() {
	log.Info("Preparing GoTi")
	prepare()
	log.Info("Starting GoTi")
	start()
}

// prepare does all the preparation work without starting anything
func prepare() {
	var options struct {
		Config   string `short:"c" long:"config" description:"Location of the configuration directory, defaults to ./configs"`
		LogLevel string `short:"l" long:"loglevel" default:"info" choice:"debug" choice:"info" choice:"warn" choice:"error" description:"Set the log level (default is INFO)"`
		Check    bool   `long:"check" description:"This will perform a pre-flight check of the configuration without starting the application"`
		Watch    bool   `short:"w" long:"watch" description:"If set config changes will be automatically watched and applied without restarting the application"`
	}

	log.Debug("Reading command line arguments")
	p := flags.NewParser(&options, flags.Default)
	if _, err := p.Parse(); err != nil {
		log.Fatal(err)
	}

	log.Debug("Setting default parameters")
	if options.Config == "" {
		dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
		if err != nil {
			log.Fatal(err)
		}
		options.Config = path.Join(dir, "configs")
	}

	level, parseErr := log.ParseLevel(options.LogLevel)
	if parseErr != nil {
		log.Fatal(parseErr)
	}
	log.SetLevel(level)

	log.Debug("Loading configuration files")
	if err := configs.Init(options.Config, options.Watch); err != nil {
		log.Fatal(err)
	}

	if options.Check {
		log.Debug("Pre-flight check done")
		os.Exit(0)
	}
}

// start establishes the connection to the database, does the migration and starts the webserver
func start() {
	log.Debug("Connecting to database")
	if err := database.Init(); err != nil {
		log.Fatal(err)
	}

	log.Debug("Start server")
	server.Start()
}
