package database

import (
	"fmt"
	"time"

	"gitlab.com/msieber88/goti/configs"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// DB represents the database connection pool
var DB *gorm.DB

// Init sets up the database connection and throws an error if it cannot be established
func Init() error {

	dsn := buildConnectString()
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		return err
	}

	sqlDB, err := db.DB()

	if err != nil {
		return err
	}

	// SetMaxIdleConns sets the maximum number of connections in the idle connection pool.
	sqlDB.SetMaxIdleConns(10)

	// SetMaxOpenConns sets the maximum number of open connections to the database.
	sqlDB.SetMaxOpenConns(100)

	// SetConnMaxLifetime sets the maximum amount of time a connection may be reused.
	sqlDB.SetConnMaxLifetime(time.Hour)

	// make the DB available
	DB = db

	return nil
}

// buildConnectString creates the connect string for the postgres database
func buildConnectString() string {
	host := configs.C.Common.Database.Host
	port := configs.C.Common.Database.Port
	db := configs.C.Common.Database.Database
	user := configs.C.Common.Database.User
	password := configs.C.Common.Database.Password

	return fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Europe/Amsterdam", host, user, password, db, port)
}
