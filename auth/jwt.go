package auth

import (
	"net/http"
	"time"

	"github.com/go-chi/jwtauth"
	"gitlab.com/msieber88/goti/configs"
)

// 15 minutes
const accessTokenExpiry = 15 * time.Minute

// 10 days
const refreshTokenExpiry = 24 * 10 * time.Hour

var tokenAuth *jwtauth.JWTAuth

// GetAuthMiddleware returns the middleware for JWT verification
func GetAuthMiddleware() func(http.Handler) http.Handler {
	return jwtauth.Verifier(tokenAuth)
}

// GetAuthenticator returns the middleware which verifies if access granted or not
func GetAuthenticator() func(http.Handler) http.Handler {
	return jwtauth.Authenticator
}

// getAccessToken creates a new access token
func getAccessToken(userID string, groupMembership []string) (string, error) {
	return getToken(userID, groupMembership, accessTokenExpiry)
}

// getRefreshToken creates a new refresh token
func getRefreshToken(userID string) (string, error) {
	return getToken(userID, nil, refreshTokenExpiry)
}

// getToken creates a new token with the entered values
func getToken(userID string, groupMembership []string, expireIn time.Duration) (string, error) {
	claims := make(map[string]interface{})
	claims["user_id"] = userID

	if groupMembership != nil {
		claims["membership"] = groupMembership
	}

	jwtauth.SetIssuedNow(claims)
	jwtauth.SetExpiryIn(claims, expireIn)
	tokenString, err := encodeToken(claims)
	return tokenString, err
}

// encodeToken takes the prepared claim and creates the jwt token from it
func encodeToken(claims map[string]interface{}) (string, error) {
	if tokenAuth == nil {
		tokenAuth = jwtauth.New("HS256", []byte(configs.C.Common.Auth.Secret), nil)
	}

	_, token, err := tokenAuth.Encode(claims)
	return token, err
}
