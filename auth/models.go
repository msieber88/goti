package auth

import "gorm.io/gorm"

// User describes the database model of a system user
type User struct {
	gorm.Model
}
