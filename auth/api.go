package auth

import (
	"net/http"

	"github.com/go-chi/chi"
	log "github.com/sirupsen/logrus"
)

// Router sets up the routes for the auth module
func Router() chi.Router {
	r := chi.NewRouter()
	r.Post("/login", login)
	r.Get("/refresh", refreshToken)
	return r
}

// login will authenticate the user and returns an access and refresh token
// if the user is valid
func login(w http.ResponseWriter, r *http.Request) {
	token, err := getAccessToken("dev", []string{"foo", "bar"})
	if err != nil {
		log.Error(err)
	}
	w.Write([]byte(token))
}

// refreshToken uses the provided JWT refresh token to create a new pair of access and refresh token
func refreshToken(w http.ResponseWriter, r *http.Request) {
	token, err := getRefreshToken("dev")
	if err != nil {
		log.Error(err)
	}
	w.Write([]byte(token))
}
