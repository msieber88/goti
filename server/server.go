package server

import (
	"net/http"

	"github.com/go-chi/chi"
	log "github.com/sirupsen/logrus"
	"gitlab.com/msieber88/goti/auth"
	"gitlab.com/msieber88/goti/configs"
)

const apiV1 = "/api/v1"

// Start initializes and starts the webserver
func Start() {
	r := chi.NewRouter()
	createRootHandlers(r)

	server := configs.C.Common.Server.Host
	port := configs.C.Common.Server.Port

	log.Infof("Starting server on %s:%s", server, port)
	log.Fatal(http.ListenAndServe(server+":"+port, r))
}

// createRootHandlers sets up the root handlers for all modules with authentication
// and instructs the module to register with the subrouter
func createRootHandlers(router *chi.Mux) {

	// Protected routes
	router.Group(func(r chi.Router) {
		// Seek, verify and validate JWT tokens
		r.Use(auth.GetAuthMiddleware())

		// Handle valid / invalid tokens.
		r.Use(auth.GetAuthenticator())

	})

	// Public routes
	router.Group(func(r chi.Router) {
		r.Get("/", func(w http.ResponseWriter, r *http.Request) {
			w.Write([]byte("welcome anonymous"))
		})

		r.Mount(apiV1+"/auth", auth.Router())
	})
}
