# GoTi

More than just another ticket system. GoTi is a support platform to improve your service desk

## Development

Recommenden development setup:
* Golang >= 1.15
* Visual Studio Code
  * Extension "golang.go"
* Docker

Start the application:

```
cd dev
docker-compose up -d
cd ..

Windows:
go build -o goti.exe cmd/goti/goti.go ; .\goti.exe

Linux:
go build -o goti cmd/goti/goti.go && ./goti
```