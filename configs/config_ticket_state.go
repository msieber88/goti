package configs

// TicketState is the configuration structure which holds the ticket states
type TicketState struct {
	Key   string  `mapstructure:"key" validate:"required"`
	State []State `mapstructure:"states" validate:"gt=0,dive"`
}

// State contains the configuration for a ticket state
type State struct {
	Key     string `mapstructure:"key" validate:"required"`
	Display string `mapstructure:"display" validate:"required"`
}
