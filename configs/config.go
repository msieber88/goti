package configs

import (
	"path"

	"github.com/creasty/defaults"
	"github.com/fsnotify/fsnotify"
	"github.com/go-playground/validator/v10"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

// Config is the application configuration structure
type Config struct {
	Common        Common          `mapstructure:"common"`
	TicketProcess []TicketProcess `mapstructure:"ticket_process" validate:"dive"`
	TicketState   []TicketState   `mapstructure:"ticket_state" validate:"dive"`
	TicketType    []TicketType    `mapstructure:"ticket_type" validate:"dive"`
}

// use a single instance of Validate, it caches struct info
var validate *validator.Validate

// list of config files to load
var configFiles = []string{"common.yaml", "ticket_type.yaml", "ticket_state.yaml", "ticket_process.yaml"}

// C holds the loaded values from the configuration file.
// Config changes will be automatically detected
var C Config

// Init is loading all configuration files from the given configuration directory
// This will automatically update the config by watching the files if the parameter
// watch is set
func Init(confDir string, watch bool) error {
	// load config files
	if err := load(confDir, watch); err != nil {
		return err
	}

	// set default values
	log.Debug("Setting default values")
	if err := defaults.Set(&C); err != nil {
		return err
	}

	return verify()
}

// load is loading the values from the configuration files and putting it in the Config struct
// watch config file changes if watch is enabled
func load(confDir string, watch bool) error {

	for _, config := range configFiles {
		file := path.Join(confDir, config)
		viper.SetConfigFile(file)

		if watch {
			viper.WatchConfig()
			viper.OnConfigChange(func(e fsnotify.Event) {
				err := validate.Struct(C)

				if err != nil {
					log.Error(err)
				}
			})
		}

		log.Debugf("Reading config file: %s", file)
		err := viper.ReadInConfig()
		err = viper.Unmarshal(&C)

		if err != nil {
			return err
		}
	}

	return nil
}

// verify checks the configuration file for semantic errors
func verify() error {
	validate = validator.New()

	// add custom validators
	validate.RegisterValidation("is-awesome", ProcessKeyExists)
	validate.RegisterValidation("process_key_exists", ProcessKeyExists)
	validate.RegisterValidation("state_key_exists", StateKeyExists)

	log.Debug("Validating configuration")
	return validate.Struct(C)
}

// ProcessKeyExists validates if the given key exists as a process key
func ProcessKeyExists(fl validator.FieldLevel) bool {
	processKey := fl.Field().String()

	// empty string is not allowed
	if processKey == "" {
		return false
	}

	for _, p := range C.TicketProcess {
		if p.Key == processKey {
			return true
		}
	}

	return false
}

// StateKeyExists validates if the given key exists as a state key
func StateKeyExists(fl validator.FieldLevel) bool {
	stateKey := fl.Field().String()

	// empty string is not allowed
	if stateKey == "" {
		return false
	}

	for _, s := range C.TicketState {
		if s.Key == stateKey {
			return true
		}
	}

	return false
}
