package configs

// TicketProcess is the configuration structure which holds the ticket process
type TicketProcess struct {
	Key         string              `mapstructure:"key" validate:"required"`
	Display     string              `mapstructure:"display" validate:"required"`
	Transitions []ProcessTransition `mapstructure:"transitions" validate:"gt=0,dive"`
}

// ProcessTransition contains the configuration for a ticket process transition
type ProcessTransition struct {
	Key       string `mapstructure:"key" validate:"required"`
	Display   string `mapstructure:"display" validate:"required"`
	FromState string `mapstructure:"from_state" validate:"required"`
	ToState   string `mapstructure:"to_state" validate:"required"`
	Condition string `mapstructure:"condition"`
}
