package configs

// TicketType is the configuration structure which holds the ticket types
type TicketType struct {
	Key        string   `mapstructure:"key" validate:"required,min=2,max=5,alpha"`
	Display    string   `mapstructure:"display" validate:"required"`
	States     string   `mapstructure:"states" validate:"required,state_key_exists"`
	Process    string   `mapstructure:"process" validate:"required,process_key_exists"`
	Priorities []string `mapstructure:"priorities" validate:"gt=0"`
	Fields     []Field  `mapstructure:"fields" validate:"dive"`
}

// Field contains the configuration for ticket custom fields
type Field struct {
	Key      string   `mapstructure:"key" validate:"required"`
	Display  string   `mapstructure:"display" validate:"required"`
	Type     string   `mapstructure:"type" validate:"required"`
	Default  string   `mapstructure:"default"`
	Required bool     `mapstructure:"required"`
	Values   []string `mapstructure:"values" validate:"required_if=Type select"`
}
