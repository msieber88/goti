package configs

// Common ist the configuration structure for the basic server configuration
type Common struct {
	Server   Server         `mapstructure:"server"`
	Database Database       `mapstructure:"database"`
	Mail     Mail           `mapstructure:"mail"`
	Auth     Authentication `mapstructure:"authentication"`
}

// Server is the server configuration structure
type Server struct {
	Host string `mapstructure:"host" validate:"hostname" default:"localhost"`
	Port string `mapstructure:"port" validate:"numeric" default:"8080"`
}

// Database is the database configuration structure
type Database struct {
	Host     string `mapstructure:"host" validate:"hostname" default:"localhost"`
	Port     string `mapstructure:"port" validate:"numeric" default:"8080"`
	Database string `mapstructure:"database" validate:"alphanum" default:"goti"`
	User     string `mapstructure:"user" validate:"required,alphanum" default:"goti"`
	Password string `mapstructure:"password"`
}

// Mail is the mail configuration structure
type Mail struct {
	Sender string `mapstructure:"sender" validate:"required,email"`
	SMTP   struct {
		Host     string `mapstructure:"host" validate:"hostname" default:"localhost"`
		Port     string `mapstructure:"port" validate:"numeric" default:"587"`
		User     string `mapstructure:"user" validate:"alphanum"`
		Password string `mapstructure:"password"`
	} `mapstructure:"smtp"`
}

// Authentication is the authentication configuration structure
type Authentication struct {
	Secret  string   `mapstructure:"secret" validate:"required,min=20"`
	Type    []string `mapstructure:"type"`
	Default struct {
		AllowRegistration bool `mapstructure:"allowRegistration" default:"true"`
	}
}
